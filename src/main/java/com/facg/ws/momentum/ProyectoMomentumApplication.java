package com.facg.ws.momentum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoMomentumApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoMomentumApplication.class, args);
	}

}
